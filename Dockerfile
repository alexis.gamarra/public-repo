FROM bellsoft/liberica-openjdk-debian:17

COPY note-api-0.0.1.jar app.jar
EXPOSE 8880
ENTRYPOINT ["java", "-Dspring.profiles.active=cloud", "-jar", "app.jar"]
