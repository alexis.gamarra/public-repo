provider "aws" {
  profile    = "default"
  region     = "us-east-1"
}

resource "aws_instance" "frontend-ec2-01" {
  ami           = "ami-06b09bfacae1453cb"
  instance_type = "t2.micro"
  tags = {
    Name = "my-company-tf"
  }
}
